angular.module('app')
  .run(['$rootScope', '$translate', '$http', function($rootScope, $translate, $http) {
    $rootScope.changeLanguage = function (langKey) {
      $translate.use(langKey);

      $http.get('/api/language')
        .then((languages) => {
          $rootScope.languages = languages;
        })
    };
  }])
