angular.module('app')
  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {
      $stateProvider
        .state('landing', {
          url: "/",
          controller: "LandingController",
          templateUrl: "views/landing.html",
        });

      $translateProvider.useUrlLoader('/api/translation', {
        queryParameter : 'localeKey'
      });

      $locationProvider.hashPrefix('!');
      $locationProvider.html5Mode(true);
      $urlRouterProvider.otherwise("/");
      $translateProvider.preferredLanguage('en');
  }]);
