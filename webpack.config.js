var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

var dist = path.join(__dirname, 'public');
var src = path.join(__dirname, 'src');

var config = {
  resolve: { extensions: ['', '.js'] },
  entry: {
    'js/app': path.join(src, 'js/app.js'),
    'css/styles': path.join(src, 'css/styles.scss'),
  },
  output: { path: dist, filename: '[name].js' },
  devtool: 'source-map',
  resolveLoader: { modulesDirectories: ['node_modules'] },
  plugins: [
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
    }),

      new ExtractTextPlugin("[name].css"),
  ],
  module: {
    loaders: [
      { test: /\.js(x?)$/, exclude: /node_modules/, loader: 'babel-loader' },
      { test: /\.scss$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader!resolve-url!sass-loader?sourceMap') },
      { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader') },
      { test: /\.html$/, loader: 'raw-loader' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader' },
      { test: /\.(png|jpg)$/, loader: 'url-loader' }
    ]
  }
};

module.exports = config;
