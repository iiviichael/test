import async from 'async';
import dbm from 'db-migrate';
import mongoose from 'mongoose';
import Language from './../server/app/models/language'
import config from './../server/config';
import translations from './data/languages.json';

exports.up = function(db, callback) {
  async.waterfall([
		connect,
		addLanguages,
		disconnect
	], (err) => {
		if(err) {
			console.log(err);
		}
		callback();
	});
};

exports.down = function(db, callback) {
  async.waterfall([
		connect,
		removeLanguages,
		disconnect
	], (err) => {
		if(err) {
			console.log(err);
		}
		callback();
	});
};

function connect(callback) {
	mongoose.connect(config.db, callback);
}

function disconnect(callback) {
	mongoose.disconnect(callback);
}

function addLanguages(callback) {
	async.eachSeries(translations, (translation, callback) => {
		const newLanguage = new Language(translation);
    newLanguage.save((err) => callback(err));
	}, (err) => callback(err))
}

function removeLanguages(callback) {
  Language.remove((err) => callback(err));
}
