import async from 'async';
import dbm from 'db-migrate';
import mongoose from 'mongoose';
import Translation from './../server/app/models/translation'
import config from './../server/config';
import translations from './data/translations.json';

exports.up = function(db, callback) {
  async.waterfall([
		connect,
		addTranslations,
		disconnect
	], (err) => {
		if(err) {
			console.log(err);
		}
		callback();
	});
};

exports.down = function(db, callback) {
  async.waterfall([
		connect,
		removeTranslations,
		disconnect
	], (err) => {
		if(err) {
			console.log(err);
		}
		callback();
	});
};

function connect(callback) {
	mongoose.connect(config.db, callback);
}

function disconnect(callback) {
	mongoose.disconnect(callback);
}

function addTranslations(callback) {
	async.eachSeries(translations, (translation, callback) => {
		const newTranslation = new Translation(translation);
    newTranslation.save((err) => callback(err));
	}, (err) => callback(err))
}

function removeTranslations(callback) {
  Translation.remove((err) => callback(err));
}
