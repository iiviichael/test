# Test project #

### Full install on Ubuntu 16.04 ###
    :::bash
    sudo apt-get update
    sudo apt-get install build-essential libssl-dev
    curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh
    bash install_nvm.sh
    source ~/.profile
    nvm install 4.2.0
    nvm use 4.2.0

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
    echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
    sudo apt-get update
    sudo apt-get install -y mongodb-org
    sudo service mongod start

    sudo npm install -g babel-cli
    sudo npm install -g db-migrate
    sudo npm install -g webpack

    npm install
    npm run db-migrate-up

### On development ###
    :::bash
    npm run dev

### To start ###
    :::bash
    npm start
