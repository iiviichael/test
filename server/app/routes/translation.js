import * as translation from './../controllers/translation'

export default function(app) {
	app.get("/api/translation", translation.get)
}
