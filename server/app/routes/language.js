import * as language from './../controllers/language'

export default function(app) {
	app.get("/api/language", language.get)
}
