import mongoose from "mongoose"

const TranslationSchema = new mongoose.Schema({
	name: {type: String, required: true},
	translation: {type: String, required: true},
	langCode: {type: String, required: true, default: 'en'}
})

export default mongoose.model("Translation", TranslationSchema)
