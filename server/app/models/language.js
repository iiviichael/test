import mongoose from "mongoose"

const LanguageSchema = new mongoose.Schema({
	name: {type: String, required: true},
	langCode: {type: String, required: true}
})

export default mongoose.model("Language", LanguageSchema)
