import _ from "lodash"
import mongoose from "mongoose"
import Translation from "../models/translation"

export function get(req, res, next) {
	Translation.find({langCode: req.query.localeKey})
    .exec(function (err, translations) {
  		if (err) {
  			res.status(500).json(err)
  		} else {
				translations = _.reduce(translations, (result, value) => {
					result[value.name] = value.translation;
					return result;
				}, {});
  			res.json(translations)
  		}
  	})
}
