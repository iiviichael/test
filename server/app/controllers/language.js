import _ from "lodash"
import mongoose from "mongoose"
import Language from "../models/language"

export function get(req, res, next) {
	Language.find()
    .exec(function (err, languages) {
  		if (err) {
  			res.status(500).json(err)
  		} else {
  			res.json(languages)
  		}
  	})
}
