"use strict";

import http from "http"
import config from "./config"
import mongoose from "mongoose"
import express from "express"
import config_express from "./config/express"

const db = mongoose.connection;
mongoose.connect(config.db);
const app = new express();
config_express(app);

http.createServer(app).listen(config.port, function() {
  console.log(`Express app started on port ${config.port}`)
});

process.on('uncaughtException', function(err) {
  console.error('Caught exception: ', err)
});

export default app;
