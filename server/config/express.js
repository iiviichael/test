"use strict";

import bodyParser from "body-parser"
import express from "express"
import fs from "fs"
import path from "path"
import mongoose from "mongoose"
import _ from "lodash"
import config from "./"

export default function (app) {
  app.set("showStackError", true);
  app.use(bodyParser.urlencoded({'extended':'true'}));
  app.use(express.static('public'))

  app.all("/*", (req, res, next) => {
    if (req.method === 'GET') {
      let curPath = req.path.split('/');
      if(curPath[1] && curPath[1] == 'api') {
        return next();
      } else {
        res.sendfile('public/index.html');
      }
    }
  });

  walkRoutes(path.join(config.root, "server", "app", "routes"));

  function walkRoutes(directory) {
    _.forEach(fs.readdirSync(directory), (file) => {
      const newPath = `${directory}/${file}`;
      const stat = fs.statSync(newPath);
      if (stat.isFile()) {
        if (/(.*)\.(js$)/.test(file)) {
          let file = require(newPath).default(app);
        }
      } else {
        if (stat.isDirectory()) {
          walkRoutes(newPath)
        }
      }
    })
  }

  app.use(function(req, res, next) {
    res.status(404).json({url: req.originalUrl, error: "Not found"});
  });

  app.use(function(err, req, res, next) {
    res.status(500).send(err.response || 'Something broken!');
  });
}
