"use strict";

const config = {
  env: process.env.NODE_ENV || "development",
  db: process.env.MONGODB_URI || "mongodb://localhost/test",
  port: process.env.PORT || 3000,
  root: `${__dirname}/../..`,
};

export default config;
